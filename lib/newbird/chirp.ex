defmodule Newbird.Chirp do
  require Meeseeks.CSS
  defstruct [:id, :author, :fullname, :body, :media]

  def by_id_author(id, author) do
    url = url(id, author)
    html = HTTPoison.get!(url).body
    fullname = get_fullname(html)
    body = get_body(html)
    media = get_media(html)
    %Newbird.Chirp{id: id, author: author, fullname: fullname, body: body, media: media}
  end

  defp url(id, author) do
    "https://mobile.twitter.com/#{author}/status/#{id}"
  end

  defp get_fullname(html) do
    html
    |> Meeseeks.one(Meeseeks.CSS.css("#main_content .user-info .fullname"))
    |> Meeseeks.Result.text()
  end

  defp get_body(html) do
    html
    |> Meeseeks.one(Meeseeks.CSS.css("#main_content .tweet-text"))
    |> Meeseeks.Result.html()
  end

  defp get_media(html) do
    html
    |> Meeseeks.one(Meeseeks.CSS.css("#main_content .card-photo .media img"))
    |> Meeseeks.Result.attr("src")
  end
end
