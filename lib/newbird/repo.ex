defmodule Newbird.Repo do
  use Ecto.Repo,
    otp_app: :newbird,
    adapter: Ecto.Adapters.Postgres
end
