defmodule NewbirdWeb.Router do
  use NewbirdWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", NewbirdWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/chirp", NewbirdWeb do
    pipe_through :browser

    get "/:author/:id", ChirpController, :chirp
  end

  # Other scopes may use custom stacks.
  # scope "/api", NewbirdWeb do
  #   pipe_through :api
  # end
end
