defmodule NewbirdWeb.ChirpController do
  use NewbirdWeb, :controller

  def chirp(conn, params) do
    IO.inspect(params)

    chirp = Newbird.Chirp.by_id_author(params["id"], params["author"])

    render(conn, "chirp.html", chirp: chirp)
  end
end
